package com.ach.bdsoportes.insertar_faltantes.model;

import java.util.Date;

public class RegMinpsBean {

	private String nroPlanillaAuditoria;
	private String docAportanteAudiroria;
	private Date fechaPagoAuditoria;
	private String idProceso;
	private Integer numeroLinea;
	private String estadoMigracion;
	
	public String getNroPlanillaAuditoria() {
		return nroPlanillaAuditoria;
	}
	public void setNroPlanillaAuditoria(String nroPlanillaAuditoria) {
		this.nroPlanillaAuditoria = nroPlanillaAuditoria;
	}
	public String getDocAportanteAudiroria() {
		return docAportanteAudiroria;
	}
	public void setDocAportanteAudiroria(String docAportanteAudiroria) {
		this.docAportanteAudiroria = docAportanteAudiroria;
	}
	public Date getFechaPagoAuditoria() {
		return fechaPagoAuditoria;
	}
	public void setFechaPagoAuditoria(Date fechaPagoAuditoria) {
		this.fechaPagoAuditoria = fechaPagoAuditoria;
	}
	public String getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(String idProceso) {
		this.idProceso = idProceso;
	}
	public Integer getNumeroLinea() {
		return numeroLinea;
	}
	public void setNumeroLinea(Integer numeroLinea) {
		this.numeroLinea = numeroLinea;
	}
	public String getEstadoMigracion() {
		return estadoMigracion;
	}
	public void setEstadoMigracion(String estadoMigracion) {
		this.estadoMigracion = estadoMigracion;
	}
	
}
