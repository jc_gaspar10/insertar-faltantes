package com.ach.bdsoportes.insertar_faltantes.model;

import java.util.Date;

/**
 * 
 * Clase encargada de mapear un registro MINPS tipo 1 activos
 * @author camilo.gaspar@alejandria-consulting.com
 */
public class RegMinps01Bean extends RegMinpsBean {
	
	
	private Integer tpReg;
	private Date fechaPago;
	private String agnoPago;
	private String mesPago;
	private String diaPago;
	private Integer nroRegsArch;
	private Integer codOperInfo;
	private Integer nroRegTp2Rep;
	private Long nroRadOPlanIntLiq;
	private Integer modalidadPlanilla;
	private String modalidadPlanillaDsc;
	private String tpPlanilla;
	private String tpPlanillaDsc;
	private String nombreApo;
	private String tpDocApo;
	private String tpDocApoDsc;
	private String nroDocApo;
	private Integer DigVerApo;
	private String codSucODepeApo;
	private String nomSucODepe;
	private String clasApo;
	private String clasApoDsc;
	private Integer natJurApo;
	private String natJurApoDsc;
	private String tpPers;
	private String tpPersDsc;
	private String formaPres;
	private String formaPresDsc;
	private String dirCorresp;
	private String ubiApo;
	private String deptoApo;
	private String mcpioApo;
	private String deptoApoDsc;
	private String mcpioApoDsc;
	private Integer codDaneActEco;
	private String codDaneActEcoDsc;
	private Long tel;
	private Long fax;
	private String email;
	private String nroDocRepLeg;
	private Integer digVerRepLeg;
	private String tpDocRepLeg;
	private String tpDocRepLegDsc;
	private String priApellRepLeg;
	private String segApellRepLeg;
	private String priNomRepLeg;
	private String segNomRepLeg;
	private String fechaIniConReesLiqCeseAct;
	private Integer tpAccion;
	private String tpAccionDsc;
	private String fechafinActCom;
	private Integer tpApoPen;
	private String tpApoPenDsc;
	private String fechaMatMerc;
	private String codDepaMatMerc;
	private String codDepaMatMercDsc;
	private String apoExoPagAporteSalSenaIcbf;
	private String apoAcogBenAporteCajCompFam;
	private Long nroPlanillaAso;
	private String FechaPagoPlanillaAso;
	private String codArlApo;
	private String codArlApoDsc;
	private String perPagoOtrosSis;
	private String perPagoSal;
	private Integer diasMora;
	private Integer nroTotEmplsOPens;
	private Long valTotNom;
	private Integer identificador;
	private String fechaActArchtp1;
	private Integer secuencia;
	private String correccion;
	
	public Integer getTpReg() {
		return tpReg;
	}
	public void setTpReg(Integer tpReg) {
		this.tpReg = tpReg;
	}
	public Date getFechaPago() {
		if(fechaPago != null){
			return fechaPago;
		}else{
			return super.getFechaPagoAuditoria();
		}
	}
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getAgnoPago() {
		return agnoPago;
	}
	public void setAgnoPago(String agnoPago) {
		this.agnoPago = agnoPago;
	}
	public String getMesPago() {
		return mesPago;
	}
	public void setMesPago(String mesPago) {
		this.mesPago = mesPago;
	}
	public String getDiaPago() {
		return diaPago;
	}
	public void setDiaPago(String diaPago) {
		this.diaPago = diaPago;
	}
	public Integer getNroRegsArch() {
		return nroRegsArch;
	}
	public void setNroRegsArch(Integer nroRegsArch) {
		this.nroRegsArch = nroRegsArch;
	}
	public Integer getCodOperInfo() {
		return codOperInfo;
	}
	public void setCodOperInfo(Integer codOperInfo) {
		this.codOperInfo = codOperInfo;
	}
	public Integer getNroRegTp2Rep() {
		return nroRegTp2Rep;
	}
	public void setNroRegTp2Rep(Integer nroRegTp2Rep) {
		this.nroRegTp2Rep = nroRegTp2Rep;
	}
	public Long getNroRadOPlanIntLiq() {
		return nroRadOPlanIntLiq;
	}
	public void setNroRadOPlanIntLiq(Long nroRadOPlanIntLiq) {
		this.nroRadOPlanIntLiq = nroRadOPlanIntLiq;
	}
	public Integer getModalidadPlanilla() {
		return modalidadPlanilla;
	}
	public void setModalidadPlanilla(Integer modalidadPlanilla) {
		this.modalidadPlanilla = modalidadPlanilla;
	}
	public String getModalidadPlanillaDsc() {
		return modalidadPlanillaDsc;
	}
	public void setModalidadPlanillaDsc(String modalidadPlanillaDsc) {
		this.modalidadPlanillaDsc = modalidadPlanillaDsc;
	}
	public String getTpPlanilla() {
		return tpPlanilla;
	}
	public void setTpPlanilla(String tpPlanilla) {
		this.tpPlanilla = tpPlanilla;
	}
	public String getTpPlanillaDsc() {
		return tpPlanillaDsc;
	}
	public void setTpPlanillaDsc(String tpPlanillaDsc) {
		this.tpPlanillaDsc = tpPlanillaDsc;
	}
	public String getNombreApo() {
		return nombreApo;
	}
	public void setNombreApo(String nombreApo) {
		this.nombreApo = nombreApo;
	}
	public String getTpDocApo() {
		return tpDocApo;
	}
	public void setTpDocApo(String tpDocApo) {
		this.tpDocApo = tpDocApo;
	}
	public String getTpDocApoDsc() {
		return tpDocApoDsc;
	}
	public void setTpDocApoDsc(String tpDocApoDsc) {
		this.tpDocApoDsc = tpDocApoDsc;
	}
	public String getNroDocApo() {
		return nroDocApo;
	}
	public void setNroDocApo(String nroDocApo) {
		this.nroDocApo = nroDocApo;
	}
	public Integer getDigVerApo() {
		return DigVerApo;
	}
	public void setDigVerApo(Integer digVerApo) {
		DigVerApo = digVerApo;
	}
	public String getCodSucODepeApo() {
		return codSucODepeApo;
	}
	public void setCodSucODepeApo(String codSucODepeApo) {
		this.codSucODepeApo = codSucODepeApo;
	}
	public String getNomSucODepe() {
		return nomSucODepe;
	}
	public void setNomSucODepe(String nomSucODepe) {
		this.nomSucODepe = nomSucODepe;
	}
	public String getClasApo() {
		return clasApo;
	}
	public void setClasApo(String clasApo) {
		this.clasApo = clasApo;
	}
	public String getClasApoDsc() {
		return clasApoDsc;
	}
	public void setClasApoDsc(String clasApoDsc) {
		this.clasApoDsc = clasApoDsc;
	}
	public Integer getNatJurApo() {
		return natJurApo;
	}
	public void setNatJurApo(Integer natJurApo) {
		this.natJurApo = natJurApo;
	}
	public String getNatJurApoDsc() {
		return natJurApoDsc;
	}
	public void setNatJurApoDsc(String natJurApoDsc) {
		this.natJurApoDsc = natJurApoDsc;
	}
	public String getTpPers() {
		return tpPers;
	}
	public void setTpPers(String tpPers) {
		this.tpPers = tpPers;
	}
	public String getTpPersDsc() {
		return tpPersDsc;
	}
	public void setTpPersDsc(String tpPersDsc) {
		this.tpPersDsc = tpPersDsc;
	}
	public String getFormaPres() {
		return formaPres;
	}
	public void setFormaPres(String formaPres) {
		this.formaPres = formaPres;
	}
	public String getFormaPresDsc() {
		return formaPresDsc;
	}
	public void setFormaPresDsc(String formaPresDsc) {
		this.formaPresDsc = formaPresDsc;
	}
	public String getDirCorresp() {
		return dirCorresp;
	}
	public void setDirCorresp(String dirCorresp) {
		this.dirCorresp = dirCorresp;
	}
	public String getUbiApo() {
		return ubiApo;
	}
	public void setUbiApo(String ubiApo) {
		this.ubiApo = ubiApo;
	}
	public String getDeptoApo() {
		return deptoApo;
	}
	public void setDeptoApo(String deptoApo) {
		this.deptoApo = deptoApo;
	}
	public String getMcpioApo() {
		return mcpioApo;
	}
	public void setMcpioApo(String mcpioApo) {
		this.mcpioApo = mcpioApo;
	}
	public String getDeptoApoDsc() {
		return deptoApoDsc;
	}
	public void setDeptoApoDsc(String deptoApoDsc) {
		this.deptoApoDsc = deptoApoDsc;
	}
	public String getMcpioApoDsc() {
		return mcpioApoDsc;
	}
	public void setMcpioApoDsc(String mcpioApoDsc) {
		this.mcpioApoDsc = mcpioApoDsc;
	}
	public Integer getCodDaneActEco() {
		return codDaneActEco;
	}
	public void setCodDaneActEco(Integer codDaneActEco) {
		this.codDaneActEco = codDaneActEco;
	}
	public String getCodDaneActEcoDsc() {
		return codDaneActEcoDsc;
	}
	public void setCodDaneActEcoDsc(String codDaneActEcoDsc) {
		this.codDaneActEcoDsc = codDaneActEcoDsc;
	}
	public Long getTel() {
		return tel;
	}
	public void setTel(Long tel) {
		this.tel = tel;
	}
	public Long getFax() {
		return fax;
	}
	public void setFax(Long fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNroDocRepLeg() {
		return nroDocRepLeg;
	}
	public void setNroDocRepLeg(String nroDocRepLeg) {
		this.nroDocRepLeg = nroDocRepLeg;
	}
	public Integer getDigVerRepLeg() {
		return digVerRepLeg;
	}
	public void setDigVerRepLeg(Integer digVerRepLeg) {
		this.digVerRepLeg = digVerRepLeg;
	}
	public String getTpDocRepLeg() {
		return tpDocRepLeg;
	}
	public void setTpDocRepLeg(String tpDocRepLeg) {
		this.tpDocRepLeg = tpDocRepLeg;
	}
	public String getTpDocRepLegDsc() {
		return tpDocRepLegDsc;
	}
	public void setTpDocRepLegDsc(String tpDocRepLegDsc) {
		this.tpDocRepLegDsc = tpDocRepLegDsc;
	}
	public String getPriApellRepLeg() {
		return priApellRepLeg;
	}
	public void setPriApellRepLeg(String priApellRepLeg) {
		this.priApellRepLeg = priApellRepLeg;
	}
	public String getSegApellRepLeg() {
		return segApellRepLeg;
	}
	public void setSegApellRepLeg(String segApellRepLeg) {
		this.segApellRepLeg = segApellRepLeg;
	}
	public String getPriNomRepLeg() {
		return priNomRepLeg;
	}
	public void setPriNomRepLeg(String priNomRepLeg) {
		this.priNomRepLeg = priNomRepLeg;
	}
	public String getSegNomRepLeg() {
		return segNomRepLeg;
	}
	public void setSegNomRepLeg(String segNomRepLeg) {
		this.segNomRepLeg = segNomRepLeg;
	}
	public String getFechaIniConReesLiqCeseAct() {
		return fechaIniConReesLiqCeseAct;
	}
	public void setFechaIniConReesLiqCeseAct(String fechaIniConReesLiqCeseAct) {
		this.fechaIniConReesLiqCeseAct = fechaIniConReesLiqCeseAct;
	}
	public Integer getTpAccion() {
		return tpAccion;
	}
	public void setTpAccion(Integer tpAccion) {
		this.tpAccion = tpAccion;
	}
	public String getTpAccionDsc() {
		return tpAccionDsc;
	}
	public void setTpAccionDsc(String tpAccionDsc) {
		this.tpAccionDsc = tpAccionDsc;
	}
	public String getFechafinActCom() {
		return fechafinActCom;
	}
	public void setFechafinActCom(String fechafinActCom) {
		this.fechafinActCom = fechafinActCom;
	}
	public Integer getTpApoPen() {
		return tpApoPen;
	}
	public void setTpApoPen(Integer tpApoPen) {
		this.tpApoPen = tpApoPen;
	}
	public String getTpApoPenDsc() {
		return tpApoPenDsc;
	}
	public void setTpApoPenDsc(String tpApoPenDsc) {
		this.tpApoPenDsc = tpApoPenDsc;
	}
	public String getFechaMatMerc() {
		return fechaMatMerc;
	}
	public void setFechaMatMerc(String fechaMatMerc) {
		this.fechaMatMerc = fechaMatMerc;
	}
	public String getCodDepaMatMerc() {
		return codDepaMatMerc;
	}
	public void setCodDepaMatMerc(String codDepaMatMerc) {
		this.codDepaMatMerc = codDepaMatMerc;
	}
	public String getCodDepaMatMercDsc() {
		return codDepaMatMercDsc;
	}
	public void setCodDepaMatMercDsc(String codDepaMatMercDsc) {
		this.codDepaMatMercDsc = codDepaMatMercDsc;
	}
	public String getApoExoPagAporteSalSenaIcbf() {
		return apoExoPagAporteSalSenaIcbf;
	}
	public void setApoExoPagAporteSalSenaIcbf(String apoExoPagAporteSalSenaIcbf) {
		this.apoExoPagAporteSalSenaIcbf = apoExoPagAporteSalSenaIcbf;
	}
	public String getApoAcogBenAporteCajCompFam() {
		return apoAcogBenAporteCajCompFam;
	}
	public void setApoAcogBenAporteCajCompFam(String apoAcogBenAporteCajCompFam) {
		this.apoAcogBenAporteCajCompFam = apoAcogBenAporteCajCompFam;
	}
	public Long getNroPlanillaAso() {
		return nroPlanillaAso;
	}
	public void setNroPlanillaAso(Long nroPlanillaAso) {
		this.nroPlanillaAso = nroPlanillaAso;
	}
	public String getFechaPagoPlanillaAso() {
		return FechaPagoPlanillaAso;
	}
	public void setFechaPagoPlanillaAso(String fechaPagoPlanillaAso) {
		FechaPagoPlanillaAso = fechaPagoPlanillaAso;
	}
	public String getCodArlApo() {
		return codArlApo;
	}
	public void setCodArlApo(String codArlApo) {
		this.codArlApo = codArlApo;
	}
	public String getCodArlApoDsc() {
		return codArlApoDsc;
	}
	public void setCodArlApoDsc(String codArlApoDsc) {
		this.codArlApoDsc = codArlApoDsc;
	}
	public String getPerPagoOtrosSis() {
		return perPagoOtrosSis;
	}
	public void setPerPagoOtrosSis(String perPagoOtrosSis) {
		this.perPagoOtrosSis = perPagoOtrosSis;
	}
	public String getPerPagoSal() {
		return perPagoSal;
	}
	public void setPerPagoSal(String perPagoSal) {
		this.perPagoSal = perPagoSal;
	}
	public Integer getDiasMora() {
		return diasMora;
	}
	public void setDiasMora(Integer diasMora) {
		this.diasMora = diasMora;
	}
	public Integer getNroTotEmplsOPens() {
		return nroTotEmplsOPens;
	}
	public void setNroTotEmplsOPens(Integer nroTotEmplsOPens) {
		this.nroTotEmplsOPens = nroTotEmplsOPens;
	}
	public Long getValTotNom() {
		return valTotNom;
	}
	public void setValTotNom(Long valTotNom) {
		this.valTotNom = valTotNom;
	}
	public int getIdentificador() {
		return identificador;
	}
	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}
	public String getFechaActArchtp1() {
		return fechaActArchtp1;
	}
	public void setFechaActArchtp1(String fechaActArchtp1) {
		this.fechaActArchtp1 = fechaActArchtp1;
	}
	public Integer getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}
	public String getCorreccion() {
		return correccion;
	}
	public void setCorreccion(String correccion) {
		this.correccion = correccion;
	}
}
