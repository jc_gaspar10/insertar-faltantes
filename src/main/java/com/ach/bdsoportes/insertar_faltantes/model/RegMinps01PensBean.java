package com.ach.bdsoportes.insertar_faltantes.model;

import java.util.Date;

/**
 * 
 * Clase encargada de mapear un registro MINPS tipo 1 pensionados
 * @author camilo.gaspar@alejandria-consulting.com
 * @author jainer.quiceno@alejandria-consulting.com
 */
public class RegMinps01PensBean extends RegMinpsBean{

	private Integer tpReg;
	private String id_apte;
	private Date fc_pago;
	private String nro_planilla;
	private Integer nro_regs;
	private String cd_operador;
	private Integer nro_regs_tp2;
	private String mod_planilla;
	private String mod_planilla_dsc;
	private String tp_planilla;
	private String tp_planilla_dsc;
	private String nm_apte;
	private String tpid_apte;
	private String tp_apte_dsc;
	private String nroid_apte;
	private String id_apte_digver;
	private String cd_sucursal;
	private String nm_sucursal;
	private String cls_apte;
	private String cls_apte_dsc;
	private String natjur_apte;
	private String natjur_apte_dsc;
	private String tppers_apte;
	private String tpid_apte_dsc;
	private String fm_pstacion;
	private String fm_pstacion_dsc;
	private String direccion;
	private String depto_apte;
	private String mcpio_apte;
	private String depto_apte_dsc;
	private String mcpio_apte_dsc;
	private String act_econ;
	private String act_econ_dsc;
	private String telefono;
	private String fax;
	private String email;
	private String nroid_replegal;
	private String id_replegal_digver;
	private String tpid_replegal;
	private String tpid_replegal_dsc;
	private String apell1_replegal;
	private String apell2_replegal;
	private String nm1_replegal;
	private String nm2_replegal;
	private String tp_apte;
	private String tppers_apte_dsc;
	private String nro_planilla_asoc;
	private String pdo_nosalud;
	private String pdo_salud;
	private Integer dias_mora;
	private Integer nro_empleados;
	private Long valor_total_nomina;
	private String identificador;
	private String fc_actualizacion;
	private Long tot_pago_pension;
	private Long tot_pago_fsp;
	private Long tot_pago_salud;
	private Long tot_pago_ccf;
	private Long tot_pago_total;
	private Integer tot_cztes_pensiones;
	private Integer tot_cztes_salud;
	private Integer tot_benef_upc;
	private Integer tot_cztes_ccf;
	private Long tot_admins_pension;
	private Long tot_admins_salud;
	private Long tot_admins_ccf;
	private String app_origen;
	private String usr_ultima_act;
	private String tmstp_ultima_act;
	private boolean tiene_minps;
	
	public Integer getTpReg() {
		return tpReg;
	}
	public void setTpReg(Integer tpReg) {
		this.tpReg = tpReg;
	}
	public String getId_apte() {
		return id_apte;
	}
	public void setId_apte(String id_apte) {
		this.id_apte = id_apte;
	}
	public Date getFc_pago() {
		return fc_pago;
	}
	public void setFc_pago(Date fc_pago) {
		this.fc_pago = fc_pago;
	}
	public String getNro_planilla() {
		return nro_planilla;
	}
	public void setNro_planilla(String nro_planilla) {
		this.nro_planilla = nro_planilla;
	}
	public Integer getNro_regs() {
		return nro_regs;
	}
	public void setNro_regs(Integer nro_regs) {
		this.nro_regs = nro_regs;
	}
	public String getCd_operador() {
		return cd_operador;
	}
	public void setCd_operador(String cd_operador) {
		this.cd_operador = cd_operador;
	}
	public Integer getNro_regs_tp2() {
		return nro_regs_tp2;
	}
	public void setNro_regs_tp2(Integer nro_regs_tp2) {
		this.nro_regs_tp2 = nro_regs_tp2;
	}
	public String getMod_planilla() {
		return mod_planilla;
	}
	public void setMod_planilla(String mod_planilla) {
		this.mod_planilla = mod_planilla;
	}
	public String getMod_planilla_dsc() {
		return mod_planilla_dsc;
	}
	public void setMod_planilla_dsc(String mod_planilla_dsc) {
		this.mod_planilla_dsc = mod_planilla_dsc;
	}
	public String getTp_planilla() {
		return tp_planilla;
	}
	public void setTp_planilla(String tp_planilla) {
		this.tp_planilla = tp_planilla;
	}
	public String getTp_planilla_dsc() {
		return tp_planilla_dsc;
	}
	public void setTp_planilla_dsc(String tp_planilla_dsc) {
		this.tp_planilla_dsc = tp_planilla_dsc;
	}
	public String getNm_apte() {
		return nm_apte;
	}
	public void setNm_apte(String nm_apte) {
		this.nm_apte = nm_apte;
	}
	public String getTpid_apte() {
		return tpid_apte;
	}
	public void setTpid_apte(String tpid_apte) {
		this.tpid_apte = tpid_apte;
	}
	public String getTp_apte_dsc() {
		return tp_apte_dsc;
	}
	public void setTp_apte_dsc(String tp_apte_dsc) {
		this.tp_apte_dsc = tp_apte_dsc;
	}
	public String getNroid_apte() {
		return nroid_apte;
	}
	public void setNroid_apte(String nroid_apte) {
		this.nroid_apte = nroid_apte;
	}
	public String getId_apte_digver() {
		return id_apte_digver;
	}
	public void setId_apte_digver(String id_apte_digver) {
		this.id_apte_digver = id_apte_digver;
	}
	public String getCd_sucursal() {
		return cd_sucursal;
	}
	public void setCd_sucursal(String cd_sucursal) {
		this.cd_sucursal = cd_sucursal;
	}
	public String getNm_sucursal() {
		return nm_sucursal;
	}
	public void setNm_sucursal(String nm_sucursal) {
		this.nm_sucursal = nm_sucursal;
	}
	public String getCls_apte() {
		return cls_apte;
	}
	public void setCls_apte(String cls_apte) {
		this.cls_apte = cls_apte;
	}
	public String getCls_apte_dsc() {
		return cls_apte_dsc;
	}
	public void setCls_apte_dsc(String cls_apte_dsc) {
		this.cls_apte_dsc = cls_apte_dsc;
	}
	public String getNatjur_apte() {
		return natjur_apte;
	}
	public void setNatjur_apte(String natjur_apte) {
		this.natjur_apte = natjur_apte;
	}
	public String getNatjur_apte_dsc() {
		return natjur_apte_dsc;
	}
	public void setNatjur_apte_dsc(String natjur_apte_dsc) {
		this.natjur_apte_dsc = natjur_apte_dsc;
	}
	public String getTppers_apte() {
		return tppers_apte;
	}
	public void setTppers_apte(String tppers_apte) {
		this.tppers_apte = tppers_apte;
	}
	public String getTpid_apte_dsc() {
		return tpid_apte_dsc;
	}
	public void setTpid_apte_dsc(String tpid_apte_dsc) {
		this.tpid_apte_dsc = tpid_apte_dsc;
	}
	public String getFm_pstacion() {
		return fm_pstacion;
	}
	public void setFm_pstacion(String fm_pstacion) {
		this.fm_pstacion = fm_pstacion;
	}
	public String getFm_pstacion_dsc() {
		return fm_pstacion_dsc;
	}
	public void setFm_pstacion_dsc(String fm_pstacion_dsc) {
		this.fm_pstacion_dsc = fm_pstacion_dsc;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getDepto_apte() {
		return depto_apte;
	}
	public void setDepto_apte(String depto_apte) {
		this.depto_apte = depto_apte;
	}
	public String getMcpio_apte() {
		return mcpio_apte;
	}
	public void setMcpio_apte(String mcpio_apte) {
		this.mcpio_apte = mcpio_apte;
	}
	public String getDepto_apte_dsc() {
		return depto_apte_dsc;
	}
	public void setDepto_apte_dsc(String depto_apte_dsc) {
		this.depto_apte_dsc = depto_apte_dsc;
	}
	public String getMcpio_apte_dsc() {
		return mcpio_apte_dsc;
	}
	public void setMcpio_apte_dsc(String mcpio_apte_dsc) {
		this.mcpio_apte_dsc = mcpio_apte_dsc;
	}
	public String getAct_econ() {
		return act_econ;
	}
	public void setAct_econ(String act_econ) {
		this.act_econ = act_econ;
	}
	public String getAct_econ_dsc() {
		return act_econ_dsc;
	}
	public void setAct_econ_dsc(String act_econ_dsc) {
		this.act_econ_dsc = act_econ_dsc;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNroid_replegal() {
		return nroid_replegal;
	}
	public void setNroid_replegal(String nroid_replegal) {
		this.nroid_replegal = nroid_replegal;
	}
	public String getId_replegal_digver() {
		return id_replegal_digver;
	}
	public void setId_replegal_digver(String id_replegal_digver) {
		this.id_replegal_digver = id_replegal_digver;
	}
	public String getTpid_replegal() {
		return tpid_replegal;
	}
	public void setTpid_replegal(String tpid_replegal) {
		this.tpid_replegal = tpid_replegal;
	}
	public String getTpid_replegal_dsc() {
		return tpid_replegal_dsc;
	}
	public void setTpid_replegal_dsc(String tpid_replegal_dsc) {
		this.tpid_replegal_dsc = tpid_replegal_dsc;
	}
	public String getApell1_replegal() {
		return apell1_replegal;
	}
	public void setApell1_replegal(String apell1_replegal) {
		this.apell1_replegal = apell1_replegal;
	}
	public String getApell2_replegal() {
		return apell2_replegal;
	}
	public void setApell2_replegal(String apell2_replegal) {
		this.apell2_replegal = apell2_replegal;
	}
	public String getNm1_replegal() {
		return nm1_replegal;
	}
	public void setNm1_replegal(String nm1_replegal) {
		this.nm1_replegal = nm1_replegal;
	}
	public String getNm2_replegal() {
		return nm2_replegal;
	}
	public void setNm2_replegal(String nm2_replegal) {
		this.nm2_replegal = nm2_replegal;
	}
	public String getTp_apte() {
		return tp_apte;
	}
	public void setTp_apte(String tp_apte) {
		this.tp_apte = tp_apte;
	}
	public String getTppers_apte_dsc() {
		return tppers_apte_dsc;
	}
	public void setTppers_apte_dsc(String tppers_apte_dsc) {
		this.tppers_apte_dsc = tppers_apte_dsc;
	}
	public String getNro_planilla_asoc() {
		return nro_planilla_asoc;
	}
	public void setNro_planilla_asoc(String nro_planilla_asoc) {
		this.nro_planilla_asoc = nro_planilla_asoc;
	}
	public String getPdo_nosalud() {
		return pdo_nosalud;
	}
	public void setPdo_nosalud(String pdo_nosalud) {
		this.pdo_nosalud = pdo_nosalud;
	}
	public String getPdo_salud() {
		return pdo_salud;
	}
	public void setPdo_salud(String pdo_salud) {
		this.pdo_salud = pdo_salud;
	}
	public Integer getDias_mora() {
		return dias_mora;
	}
	public void setDias_mora(Integer dias_mora) {
		this.dias_mora = dias_mora;
	}
	public Integer getNro_empleados() {
		return nro_empleados;
	}
	public void setNro_empleados(Integer nro_empleados) {
		this.nro_empleados = nro_empleados;
	}
	public Long getValor_total_nomina() {
		return valor_total_nomina;
	}
	public void setValor_total_nomina(Long valor_total_nomina) {
		this.valor_total_nomina = valor_total_nomina;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getFc_actualizacion() {
		return fc_actualizacion;
	}
	public void setFc_actualizacion(String fc_actualizacion) {
		this.fc_actualizacion = fc_actualizacion;
	}
	public Long getTot_pago_pension() {
		return tot_pago_pension;
	}
	public void setTot_pago_pension(Long tot_pago_pension) {
		this.tot_pago_pension = tot_pago_pension;
	}
	public Long getTot_pago_fsp() {
		return tot_pago_fsp;
	}
	public void setTot_pago_fsp(Long tot_pago_fsp) {
		this.tot_pago_fsp = tot_pago_fsp;
	}
	public Long getTot_pago_salud() {
		return tot_pago_salud;
	}
	public void setTot_pago_salud(Long tot_pago_salud) {
		this.tot_pago_salud = tot_pago_salud;
	}
	public Long getTot_pago_ccf() {
		return tot_pago_ccf;
	}
	public void setTot_pago_ccf(Long tot_pago_ccf) {
		this.tot_pago_ccf = tot_pago_ccf;
	}
	public Long getTot_pago_total() {
		return tot_pago_total;
	}
	public void setTot_pago_total(Long tot_pago_total) {
		this.tot_pago_total = tot_pago_total;
	}
	public Integer getTot_cztes_pensiones() {
		return tot_cztes_pensiones;
	}
	public void setTot_cztes_pensiones(Integer tot_cztes_pensiones) {
		this.tot_cztes_pensiones = tot_cztes_pensiones;
	}
	public Integer getTot_cztes_salud() {
		return tot_cztes_salud;
	}
	public void setTot_cztes_salud(Integer tot_cztes_salud) {
		this.tot_cztes_salud = tot_cztes_salud;
	}
	public Integer getTot_benef_upc() {
		return tot_benef_upc;
	}
	public void setTot_benef_upc(Integer tot_benef_upc) {
		this.tot_benef_upc = tot_benef_upc;
	}
	public Integer getTot_cztes_ccf() {
		return tot_cztes_ccf;
	}
	public void setTot_cztes_ccf(Integer tot_cztes_ccf) {
		this.tot_cztes_ccf = tot_cztes_ccf;
	}
	public Long getTot_admins_pension() {
		return tot_admins_pension;
	}
	public void setTot_admins_pension(Long tot_admins_pension) {
		this.tot_admins_pension = tot_admins_pension;
	}
	public Long getTot_admins_salud() {
		return tot_admins_salud;
	}
	public void setTot_admins_salud(Long tot_admins_salud) {
		this.tot_admins_salud = tot_admins_salud;
	}
	public Long getTot_admins_ccf() {
		return tot_admins_ccf;
	}
	public void setTot_admins_ccf(Long tot_admins_ccf) {
		this.tot_admins_ccf = tot_admins_ccf;
	}
	public String getApp_origen() {
		return app_origen;
	}
	public void setApp_origen(String app_origen) {
		this.app_origen = app_origen;
	}
	public String getUsr_ultima_act() {
		return usr_ultima_act;
	}
	public void setUsr_ultima_act(String usr_ultima_act) {
		this.usr_ultima_act = usr_ultima_act;
	}
	public String getTmstp_ultima_act() {
		return tmstp_ultima_act;
	}
	public void setTmstp_ultima_act(String tmstp_ultima_act) {
		this.tmstp_ultima_act = tmstp_ultima_act;
	}
	public boolean isTiene_minps() {
		return tiene_minps;
	}
	public void setTiene_minps(boolean tiene_minps) {
		this.tiene_minps = tiene_minps;
	}
}
