package com.ach.bdsoportes.insertar_faltantes.persistence.cassandra;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ach.bdsoportes.insertar_faltantes.exceptions.BDSoportesException;
import com.ach.bdsoportes.insertar_faltantes.utils.ApplicationProperties;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.LoadBalancingPolicy;
import com.datastax.driver.core.policies.RoundRobinPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;

/**
 * Clase encargada que manejar la conexi&oacute;n a la base de datos Cassandra.
 * 
 * @author jainer.quiceno@alejandria-consulting.com
 *
 */
public class CassandraConnectionFactory {

	private final static Logger logger = LogManager.getLogger(CassandraConnectionFactory.class);
	
	private static Session wSession;
	private static Session rSession;
	private static Cluster wCluster;
	private static Cluster rCluster;
	
	public static Session getWriteConnection () throws BDSoportesException {
		if (wSession == null) {
			try {
				logger.debug("antes de obtener la conexion");
				
				wCluster = Cluster.builder()
						.withClusterName(ApplicationProperties.getInstance().getValorPropiedad("cassandra.write.cluster"))
						.withCredentials(
								ApplicationProperties.getInstance().getValorPropiedad("cassandra.write.user"), 
								ApplicationProperties.getInstance().getValorPropiedad("cassandra.write.pass"))
						.withPort(Integer.parseInt(ApplicationProperties.getInstance().getValorPropiedad("cassandra.write.port")))
						.addContactPoints(ApplicationProperties.getInstance().getValorPropiedad("cassandra.write.ips").split(","))
						.withLoadBalancingPolicy(getBalancingPolicy(
								ApplicationProperties.getInstance().getValorPropiedad("cassandra.write.balancing.policy")))
						.withQueryOptions(new QueryOptions()
								.setConsistencyLevel(ConsistencyLevel.valueOf(ApplicationProperties.getInstance()
										.getValorPropiedad("cassandra.write.consistency.level"))))
						.build();
				
			    wSession = wCluster.connect();
			    logger.debug("despues de obtener la conexion");
			} catch (Exception e) {
				logger.error("excepcion obteniendo conexion:",e);
				throw new BDSoportesException("Error obteniendo la conexion", e);
			}
		}
		return wSession;
	}
	
	public static Session getReadConnection () throws BDSoportesException {
		if (rSession == null) {
			try {
				logger.debug("antes de obtener la conexion");
				
				rCluster = Cluster.builder()
						.withClusterName(ApplicationProperties.getInstance().getValorPropiedad("cassandra.read.cluster"))
						.withCredentials(
								ApplicationProperties.getInstance().getValorPropiedad("cassandra.read.user"), 
								ApplicationProperties.getInstance().getValorPropiedad("cassandra.read.pass"))
						.withPort(Integer.parseInt(ApplicationProperties.getInstance().getValorPropiedad("cassandra.read.port")))
						.addContactPoints(ApplicationProperties.getInstance().getValorPropiedad("cassandra.read.ips").split(","))
						.withLoadBalancingPolicy(getBalancingPolicy(
								ApplicationProperties.getInstance().getValorPropiedad("cassandra.read.balancing.policy")))
						.withQueryOptions(new QueryOptions()
								.setConsistencyLevel(ConsistencyLevel.valueOf(ApplicationProperties.getInstance()
												.getValorPropiedad("cassandra.read.consistency.level"))))
						.build();
				
			    rSession = rCluster.connect();
			    logger.debug("despues de obtener la conexion");
			} catch (Exception e) {
				logger.error("excepcion obteniendo conexion:",e);
				throw new BDSoportesException("Error obteniendo la conexion", e);
			}
		}
		return rSession;
	}
	
	private static LoadBalancingPolicy getBalancingPolicy(String nombre) throws BDSoportesException{
		
		if(nombre.equals("DCAwareRoundRobinPolicy")){
			return DCAwareRoundRobinPolicy.builder().build();
		}
		else if(nombre.equals("RoundRobinPolicy")){
			RoundRobinPolicy rrp = new RoundRobinPolicy();
			return rrp;
		}
		else if(nombre.equals("TokenAwarePolicy_dc")){
			TokenAwarePolicy tapd = new TokenAwarePolicy(DCAwareRoundRobinPolicy.builder().build());
			return tapd;
		}
		else if(nombre.equals("TokenAwarePolicy_rr")){
			TokenAwarePolicy tapr = new TokenAwarePolicy(new RoundRobinPolicy());
			return tapr;
		}
		else
		{
			throw new BDSoportesException("Politica de balanceo no soportada: "+nombre);
		}
	}
	
	 public static void closeWrite() {
		 if (wSession != null) {
			 wSession.close(); 
		 }
		 if (wCluster != null) {
			 wCluster.close(); 
		 }
	 }
	 
	 public static void closeRead() {
		 if (rSession != null) {
			 rSession.close(); 
		 }
		 if (rCluster != null) {
			 rCluster.close(); 
		 }
	 }
}
