package com.ach.bdsoportes.insertar_faltantes.persistence.cassandra;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ach.bdsoportes.insertar_faltantes.exceptions.BDSoportesException;
import com.ach.bdsoportes.insertar_faltantes.model.RegMinps01PensBean;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.LocalDate;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.CodecNotFoundException;
import com.datastax.driver.core.exceptions.InvalidQueryException;

/**
 * Clase encargada de insertar la información obtenida de la ruta a los archivos MINPS en la tabla
 *  pen_planilla_apte_tots
 * @author camilo.gaspar@alejandria-consulting.com
 *
 */
public class FaltantesPenUpsertDAO {

	/**
	 * Instancia de log4j encargada de escribir en el log
	 */
	private static final Logger logger = LogManager.getLogger(FaltantesPenUpsertDAO.class);

	/**
	 * Atributo que representa la única instancia que se creará de la clase actual
	 */
	private static FaltantesPenUpsertDAO INSTANCE = null;

	/**
	 * Atributo encargado de mantener la conexión al cluster de Cassandra
	 */
	private Session session = null;

	/**
	 * Representa una declaración preparada, una consulta con variables enlazadas que se preparará
	 */
	private PreparedStatement statementInsPenFaltante = null;

	/**
	 * Declaración que se ejecutará para insertar la información en la base de datos Cassandra
	 */
	private static final String INSERT_REGS_TP_1 = 
			"INSERT INTO sopsoi.pen_planilla_apte_tots "
					+ "(fc_pago, nro_planilla, id_apte, cd_operador, mod_planilla, pdo_salud, tiene_minps) "
					+ "VALUES (?,?,?,?,?,?,?)";


	/**
	 * Método constructor de la clase que se encarga de obtener la conexión a la base de datos
	 *  y de prepara la declaración que se ejecutará posteriormente
	 * @throws BDSoportesException
	 */
	private FaltantesPenUpsertDAO() throws BDSoportesException{
		
		if(this.session == null){
			this.session = CassandraConnectionFactory.getWriteConnection();
		}

		if(statementInsPenFaltante == null){
			statementInsPenFaltante = session.prepare(INSERT_REGS_TP_1);
		}
	}

	/**
	 * Método encargado de devolver la única instancia de la clase actual.
	 * <br>
	 * Si la instancia no ha sido creada, crea una nueva y <strong>ÚNICA</strong> instancia.
	 * @return Única instancia de la clase actual
	 * @throws BDSoportesException Si se produce un error al obtener la conexión a la base de datos
	 */
	public static FaltantesPenUpsertDAO getInstance() throws BDSoportesException{

		if(INSTANCE == null){
			INSTANCE = new FaltantesPenUpsertDAO();
		}

		return INSTANCE;
	}

	/**
	 * Método encargado de anexar la información necesaria a la declaración {@link FaltantesPenUpsertDAO#statementInsPenFaltante} 
	 *  y luego ejecutarla.
	 * @param regTp1 Instancia de la clase {@link RegMinps01PensBean} que contiene la información a insertar
	 * @throws BDSoportesException Si se produce algún error en la ejecución de la declaración
	 */
	public void insertarRegTp1Pen(RegMinps01PensBean regTp1) throws BDSoportesException{

		try{

			BoundStatement bound = statementInsPenFaltante.bind()
					.setDate("fc_pago", LocalDate.fromMillisSinceEpoch(regTp1.getFc_pago().getTime()))//fec_pago
					.setString("nro_planilla", regTp1.getNroPlanillaAuditoria())//id_apte
					.setString("id_apte", regTp1.getDocAportanteAudiroria());//nro_planilla;

			if(regTp1.getCd_operador() != null){
				bound.setString("cd_operador", regTp1.getCd_operador());
			}else{
				bound.setToNull("cd_operador");
			}
			if(regTp1.getMod_planilla() != null){
				bound.setString("mod_planilla", regTp1.getMod_planilla());
			}else{
				bound.setToNull("mod_planilla");
			}
			if(regTp1.getPdo_salud() != null){
				bound.setString("pdo_salud", regTp1.getPdo_salud());
			}else{
				bound.setToNull("pdo_salud");
			}
			bound.setBool("tiene_minps", false);

			session.execute(bound);

		}catch (InvalidQueryException ex) {
			logger.error("Error insertando campo NULL en el registro tp1 Pensionado: " + ex.getMessage(), ex);
			throw new BDSoportesException( "Error insertando en campo NULL en el registro tp1 Pensionado", 
					regTp1.getNroPlanillaAuditoria(), 
					regTp1.getDocAportanteAudiroria(), 
					regTp1.getFechaPagoAuditoria(),ex);
		}catch (CodecNotFoundException e) {
			logger.error("Error insertando registro tp1 diferencia de tipos Pensionado: " + e.getMessage(), e);
			throw new BDSoportesException( "Error insertando registro tp1 diferencia de tipos Pensionado", 
					regTp1.getNroPlanillaAuditoria(), 
					regTp1.getDocAportanteAudiroria(), 
					regTp1.getFechaPagoAuditoria(),e);
		}catch (Exception e) {
			logger.error("Error insertando registro tp1 no identificado Pensionado: " + e.getMessage(), e);
			throw new BDSoportesException( "Error insertando registro tp1 no identificado Pensionado", 
					regTp1.getNroPlanillaAuditoria(), 
					regTp1.getDocAportanteAudiroria(), 
					regTp1.getFechaPagoAuditoria(),e);
		}

	}


}
