package com.ach.bdsoportes.insertar_faltantes.persistence.cassandra;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ach.bdsoportes.insertar_faltantes.exceptions.BDSoportesException;
import com.ach.bdsoportes.insertar_faltantes.model.RegMinps01Bean;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.LocalDate;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.CodecNotFoundException;
import com.datastax.driver.core.exceptions.InvalidQueryException;

/**
 * Clase encargada de insertar la información obtenida de la ruta a los archivos MINPS en la tabla
 *  act_planilla_apte_tots
 * @author camilo.gaspar@alejandria-consulting.com
 *
 */
public class FaltantesActUpsertDAO {

	/**
	 * Instancia de log4j encargada de escribir en el log
	 */
	private static final Logger logger = LogManager.getLogger(FaltantesActUpsertDAO.class);

	/**
	 * Atributo que representa la única instancia que se creará de la clase actual
	 */
	private static FaltantesActUpsertDAO INSTANCE = null;

	/**
	 * Atributo encargado de mantener la conexión al cluster de Cassandra
	 */
	private Session session = null;

	/**
	 * Representa una declaración preparada, una consulta con variables enlazadas que se preparará
	 */
	private PreparedStatement statementInsActFaltante = null;

	/**
	 * Declaración que se ejecutará para insertar la información en la base de datos Cassandra
	 */
	private static final String INSERT_REGS_TP_1 = 
			"INSERT INTO sopsoi.act_planilla_apte_tots "
					+ "(fc_pago, nro_planilla, id_apte, cd_operador, mod_planilla, pdo_salud, tiene_minps) "
					+ "VALUES (?,?,?,?,?,?,?)";

	/**
	 * Método constructor de la clase que se encarga de obtener la conexión a la base de datos
	 *  y de prepara la declaración que se ejecutará posteriormente
	 * @throws BDSoportesException
	 */
	private FaltantesActUpsertDAO() throws BDSoportesException{

		if(this.session == null){
			this.session = CassandraConnectionFactory.getWriteConnection();
		}

		if(statementInsActFaltante == null){
			statementInsActFaltante = session.prepare(INSERT_REGS_TP_1);
		}
	}

	/**
	 * Método encargado de devolver la única instancia de la clase actual.
	 * <br>
	 * Si la instancia no ha sido creada, crea una nueva y <strong>ÚNICA</strong> instancia.
	 * @return Única instancia de la clase actual
	 * @throws BDSoportesException Si se produce un error al obtener la conexión a la base de datos
	 */
	public static FaltantesActUpsertDAO getInstance() throws BDSoportesException{

		if(INSTANCE == null){
			INSTANCE = new FaltantesActUpsertDAO();
		}

		return INSTANCE;
	}

	/**
	 * Método encargado de anexar la información necesaria a la declaración {@link FaltantesActUpsertDAO#statementInsActFaltante} 
	 *  y luego ejecutarla.
	 * @param regTp1 Instancia de la clase {@link RegMinps01Bean} que contiene la información a insertar
	 * @throws BDSoportesException Si se produce algún error en la ejecución de la declaración
	 */
	public void insertarRegTp1Act(RegMinps01Bean regTp1) throws BDSoportesException{

		try{

			BoundStatement bound = statementInsActFaltante.bind()
					.setDate("fc_pago", LocalDate.fromMillisSinceEpoch(regTp1.getFechaPago().getTime()))
					.setString("nro_planilla", regTp1.getNroPlanillaAuditoria())
					.setString("id_apte", regTp1.getDocAportanteAudiroria());

			if(regTp1.getCodOperInfo() != null){
				bound.setString("cd_operador", String.valueOf(regTp1.getCodOperInfo()));
			}else{
				bound.setToNull("cd_operador");
			}
			if(regTp1.getModalidadPlanilla() != null){
				bound.setString("mod_planilla", String.valueOf(regTp1.getModalidadPlanilla()));
			}else{
				bound.setToNull("mod_planilla");
			}			
			if(regTp1.getPerPagoSal() != null){
				bound.setString("pdo_salud",regTp1.getPerPagoSal());
			}else{
				bound.setToNull("pdo_salud");
			}

			bound.setBool("tiene_minps", false);
			session.execute(bound);

		}catch (InvalidQueryException ex) {
			logger.error("Error insertando campo NULL en el registro tp1 Activos:" + ex.getMessage(), ex);
			throw new BDSoportesException( "Error insertando en campo NULL en el registro tp1 Activos", 
					regTp1.getNroPlanillaAuditoria(), 
					regTp1.getDocAportanteAudiroria(), 
					regTp1.getFechaPago(),ex);
		}catch (CodecNotFoundException e) {
			logger.error("Error insertando registro tp1 diferencia de tipos Activos: " + e.getMessage(), e);
			throw new BDSoportesException( "Error insertando registro tp1 diferencia de tipos Activos", 
					regTp1.getNroPlanillaAuditoria(), 
					regTp1.getDocAportanteAudiroria(), 
					regTp1.getFechaPago(),e);
		}catch (Exception e) {
			logger.error("Error insertando registro tp1 no identificado Activos: " + e.getMessage(), e);
			throw new BDSoportesException( "Error insertando registro tp1 no identificado Activos", 
					regTp1.getNroPlanillaAuditoria(), 
					regTp1.getDocAportanteAudiroria(), 
					regTp1.getFechaPago(),e);
		}


	}


}
