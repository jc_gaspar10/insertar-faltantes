package com.ach.bdsoportes.insertar_faltantes.enums;

/**
 * 
 * Estados de los diferentes procesos de ejecucion de transformación
 * 
 * @author jairo@alejandria-consulting.com
 * @data 2017-11-08
 *
 */
public enum EstadoProcesoEnum {

	OK, ERROR, ADVERTENCIA;
}
