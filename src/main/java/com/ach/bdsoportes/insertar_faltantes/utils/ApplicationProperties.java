package com.ach.bdsoportes.insertar_faltantes.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Clase encargada de obtener información del archivo de propiedades que se pasa como parámetro con un -Dconfiguration.properties
 * @author camilo.gaspar@alejandria-consulting.com
 *
 */
public class ApplicationProperties {

	/**
	 * Instancia de log4j encargada de escribir en el log
	 */
	private static final Logger logger = LogManager.getLogger(ApplicationProperties.class);

	/**
	 * Constante que contiene el nombre de la propiedad del sistema en la que se buscara el archivo 
	 *  de propiedades
	 */
	private static final String CONFIGURATION_PROPERTIES = "configuration.properties";

	/**
	 * Atributo que representa la única instancia que se creará de la clase actual
	 */
	private static ApplicationProperties INSTANCE = null;

	/**
	 * Archivo de propiedades que contiene las configuraciones de la aplicación
	 */
	private Properties properties = null;


	/**
	 * Constructor de la clase que se encarga de crear una <strong>ÚNICA<strong> instancia y de cargar el archivo de propiedades
	 */
	private ApplicationProperties(){

		if(properties == null){
			properties = new Properties();

			String rutaProperties = System.getProperty(CONFIGURATION_PROPERTIES);
			if(rutaProperties == null || rutaProperties.equals("")){

				logger.error("Error no se pasó la ruta del archivo de propiedades en la propiedad \"-Dconfiguration.properties\" . ");
			}else{
				try{
					FileInputStream path = new FileInputStream(rutaProperties); 
					properties.load(path);
				} catch (FileNotFoundException e) {
					logger.error("Error al intentar abrir el archivo de propiedades "+rutaProperties,e);
				} catch (IOException e) {
					logger.error("Error al intentar cargar el archivo de propiedades.",e);
				}
			}
		}
	}



	/**
	 * Método encargado de devolver una única instancia de la clase {@link ApplicationProperties} 
	 * @return Instancia única de la clase {@link ApplicationProperties}
	 */
	public static synchronized ApplicationProperties getInstance() {

		if(INSTANCE == null){
			INSTANCE = new ApplicationProperties();
		}

		return INSTANCE;
	}

	/**
	 * Método encargado de obtener el valor de la propiedad dada como parámetro 
	 * @param llave propiedad de la cual se desea saber el valor
	 * @return Valor de la propiedad pasado por parámetro
	 */
	public String getValorPropiedad(String llave) {
		logger.debug("Obteniendo propiedad "+llave);
		return properties.getProperty(llave);
	}

}
