package com.ach.bdsoportes.insertar_faltantes.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ach.bdsoportes.insertar_faltantes.exceptions.BDSoportesException;
import com.ach.bdsoportes.insertar_faltantes.model.RegMinps01Bean;
import com.ach.bdsoportes.insertar_faltantes.model.RegMinps01PensBean;
import com.ach.bdsoportes.insertar_faltantes.persistence.cassandra.CassandraConnectionFactory;
import com.ach.bdsoportes.insertar_faltantes.persistence.cassandra.FaltantesActUpsertDAO;
import com.ach.bdsoportes.insertar_faltantes.persistence.cassandra.FaltantesPenUpsertDAO;

/**
 * Clase encargada de insertar en una base de datos Cassandra la información que se encuentra en la ruta de los archivos MINPS 
 * @author camilo.gaspar@alejandria-consulting.com
 *
 */
public class InsertarFaltantesMain {


	/**
	 * Instancia de log4j encargada de escribir en el log
	 */
	private final static Logger logger = LogManager.getLogger(InsertarFaltantesMain.class);

	/**
	 * Método principal de la clase encargado de realizar las validaciones de los argumentos
	 * @param args Archivo de texto que contiene la ruta de los archivos MINPS
	 */
	public static void main(String[] args) {

		logger.info("Iniciando proceso de insercción");
		long startTime = System.currentTimeMillis();

		if(args.length == 0){

			logger.error("No se adjuntó archivo csv como parámetro.");
		}
		else if(args.length > 0){

			if(args.length >1){
				logger.warn("Se agregó más de 1 archivo, se utilizará solo el primero");
			}
			if(!(args[0].endsWith(".txt"))){
				logger.warn("La ruta dada no corresponde a un archivo txt, sin embargo, se intentará utilizar.");
			}
			File file = new File(args[0]);

			if(file != null && file.exists()){

				insertar(file);

			} 
			else{
				logger.error("El archivo "+args[0]+" no existe.");
			}
		}

		long endTime = System.currentTimeMillis();
		long duration = (endTime - startTime)/1000;
		int hours = (int) duration / 3600;
		int remainder = (int) duration - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;
		int secs = remainder;

		logger.info("El proceso de inserción ha finalizado y su duración fue de "
				+hours+(hours==1?" hora, ":" horas, ")+mins+(mins==1?" minuto":" minutos")+" y "+secs+(secs==1?" segundo.":" segundos."));

	}


	/**
	 * Método encargado de leer el archivo que se da por parámetro y llamar al método que los inserta 
	 * @param file Archivo de texto que contiene la ruta de los archivos MINPS
	 */
	private static void insertar(File file){


		FileReader fr = null;
		BufferedReader br = null;
		int line = 1;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String linea = br.readLine();

			while(linea != null){

				logger.debug("Leyendo línea "+line+" del archivo "+file.getName());
				
				if(linea.equals("")){
					logger.error("La línea "+line+" se encuentra vacía");
				}else{
					String pattern = Pattern.quote(System.getProperty("file.separator"));
					try{

						String[] rutaLinea = linea.split(pattern);

						if(rutaLinea != null && rutaLinea.length > 1){
							insertarLinea(rutaLinea[rutaLinea.length-1]);
						}else{
							logger.error("No se realizó el split ("+pattern+") como se esperaba a la linea "+line
									+" que contine la ruta: "+ linea);
						}
					}catch(PatternSyntaxException e){
						logger.error("Error realizando el split de la linea "+line+" que contiene la ruta del archivo: "+linea,e);
					}
				}
				logger.debug("Finalizó lectura de la línea "+line+" del archivo "+file.getName());
				linea = br.readLine();
				line ++;
			}
			
			if(line == 1){
				logger.error("El archivo "+file.getName()+" se encuentra vacio");
			}else{
				logger.debug("Finalizó la lectura del archivo: "+file.getName());
			}
			CassandraConnectionFactory.closeRead();
			CassandraConnectionFactory.closeWrite();


		} catch (FileNotFoundException e) {
			logger.error("Error al abrir el archivo para su lectura.",e);
		} catch (IOException e) {
			logger.error("Error al leer la linea "+line+" del archivo csv.",e);
		} finally {
			try{
				if(fr != null){
					fr.close();
				}
				if(br != null){ 
					br.close();
				}
			} catch (IOException e) {
				logger.error("Error al cerrar los recursos de lectura de archivos",e);
			}
		}
	}


	/**
	 * Método encargado de revisar si la ruta al archivo MINPS se encuentra completa
	 * @param linea Ruta al archivo MINPS
	 */
	private static void insertarLinea(String linea) {

		try{

			String[] datosRegistro = linea.split("_");

			if(datosRegistro.length == 9){
				insertarRutaCompleta(datosRegistro);
			}
			else if(datosRegistro.length == 7){
				insertarRutaIncompleta(datosRegistro);
			}
			else{
				logger.error("Ruta de archivo MINPS no soportada: "+linea);
			}
		}
		catch(PatternSyntaxException e){
			logger.error("Error al realizar el split de la ruta: "+linea,e);
		}
	}

	/**
	 * Método encargado de modelar los datos que se encuentran en la ruta al archivo MINPS cuando no es completa
	 * @param datosRegistro Datos obtenidos de la ruta al archivo MINPS
	 */ 
	private static void insertarRutaIncompleta(String[] datosRegistro) {

		String tipo = datosRegistro[6];
		if(tipo.contains(".")){
			tipo = tipo.substring(0, tipo.lastIndexOf('.'));
		}

		if(tipo.equals("I")){

			RegMinps01Bean regTp01 =  new RegMinps01Bean();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date fechaPago = null;
			String fecha = datosRegistro[0];
			if(fecha.length()==8){
				fecha = fecha.substring(0, 4)+"-"+fecha.substring(4,6)+"-"+fecha.substring(6);
			}
			try {
				fechaPago = sdf.parse(fecha);
				regTp01.setFechaPago(fechaPago);
				regTp01.setNroPlanillaAuditoria(datosRegistro[1]);
				regTp01.setTpDocApo(datosRegistro[2]);
				regTp01.setNroDocApo(datosRegistro[3]);
				regTp01.setDocAportanteAudiroria(regTp01.getTpDocApo()+regTp01.getNroDocApo());
				regTp01.setCodOperInfo(Integer.parseInt(datosRegistro[5]));

				insertarAct(regTp01);


			} catch (ParseException e) {
				logger.error("Error al volver al transformar la fecha "+fecha+" a tipo Date.",e);
			}
		}
		else if(tipo.equals("IP")){

			RegMinps01PensBean regTp01 =  new RegMinps01PensBean();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date fechaPago = null;
			String fecha = datosRegistro[0];
			if(fecha.length()==8){
				fecha = fecha.substring(0, 4)+"-"+fecha.substring(4,6)+"-"+fecha.substring(6);
			}
			try {
				fechaPago = sdf.parse(fecha);
				regTp01.setFc_pago(fechaPago);
				regTp01.setNroPlanillaAuditoria(datosRegistro[1]);
				regTp01.setTpid_apte(datosRegistro[2]);
				regTp01.setNm_apte(datosRegistro[3]);
				regTp01.setDocAportanteAudiroria(regTp01.getTpid_apte()+regTp01.getNm_apte());
				regTp01.setCd_operador(datosRegistro[5]);

				insertarPen(regTp01);


			} catch (ParseException e) {
				logger.error("Error al volver al transformar la fecha "+fecha+" a tipo Date.",e);
			}
		}else{
			logger.error("El valor del campo pensionado "+tipo+" no es válido.");
		}
	}


	/**
	 * Método encargado de modelar los datos que se encuentran en la ruta al archivo MINPS cuando es completa
	 * @param datosRegistro Datos obtenidos de la ruta al archivo MINPS
	 */
	private static void insertarRutaCompleta(String[] datosRegistro) {

		String tipo = datosRegistro[7];

		if(tipo.equals("I")){

			RegMinps01Bean regTp01 =  new RegMinps01Bean();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date fechaPago = null;
			String fecha = datosRegistro[0];
			if(fecha.length()==8){
				fecha = fecha.substring(0, 4)+"-"+fecha.substring(4,6)+"-"+fecha.substring(6);
			}
			try {
				fechaPago = sdf.parse(fecha);
				regTp01.setFechaPago(fechaPago);
				regTp01.setModalidadPlanilla(Integer.parseInt(datosRegistro[1]));
				regTp01.setNroPlanillaAuditoria(datosRegistro[2]);
				regTp01.setTpDocApo(datosRegistro[3]);
				regTp01.setNroDocApo(datosRegistro[4]);
				regTp01.setDocAportanteAudiroria(regTp01.getTpDocApo()+regTp01.getNroDocApo());
				regTp01.setCodOperInfo(Integer.parseInt(datosRegistro[6]));
				if(datosRegistro[8].contains(".")){
					regTp01.setPerPagoSal(datosRegistro[8].substring(0, datosRegistro[8].lastIndexOf('.')));
				}else{
					logger.error("Se esperaba que la cadena "+datosRegistro[8]+" tuviera un punto. "
							+ "Campo periodo pago se insertará nulo");
				}

				insertarAct(regTp01);


			} catch (ParseException e) {
				logger.error("Error al volver al transformar la fecha "+fecha+" a tipo Date.",e);
			}
		}
		else if(tipo.equals("IP")){

			RegMinps01PensBean regTp01 =  new RegMinps01PensBean();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date fechaPago = null;
			String fecha = datosRegistro[0];
			if(fecha.length()==8){
				fecha = fecha.substring(0, 4)+"-"+fecha.substring(4,6)+"-"+fecha.substring(6);
			}
			try {
				fechaPago = sdf.parse(fecha);
				regTp01.setFc_pago(fechaPago);
				regTp01.setMod_planilla(datosRegistro[1]);
				regTp01.setNroPlanillaAuditoria(datosRegistro[2]);
				regTp01.setTpid_apte(datosRegistro[3]);
				regTp01.setNm_apte(datosRegistro[4]);
				regTp01.setDocAportanteAudiroria(regTp01.getTpid_apte()+regTp01.getNm_apte());
				regTp01.setCd_operador(datosRegistro[6]);
				regTp01.setPdo_salud(datosRegistro[8].substring(0, datosRegistro[8].lastIndexOf('.')));

				insertarPen(regTp01);


			} catch (ParseException e) {
				logger.error("Error al volver al transformar la fecha "+fecha+" a tipo Date.",e);
			}
		}else{
			logger.error("El valor del campo pensionado "+tipo+" no es válido.");
		}
	}

	/**
	 * Método encargado de obtener la instancia de la clase que inserta la información 
	 *  de los pensionados en la tabla pen_planilla_apte_tots 
	 * @param regTp01 Instancia de la clase {@link RegMinps01PensBean} que contiene la información 
	 *  que se logro obtener de la ruta al archivo MINPS
	 */
	private static void insertarPen(RegMinps01PensBean regTp01) {

		FaltantesPenUpsertDAO dao = null;

		try {

			dao = FaltantesPenUpsertDAO.getInstance();
			if(dao != null){
				dao.insertarRegTp1Pen(regTp01);
			}else{
				logger.error("Error obteniendo instancia de la clase que inserta la información de pensionados en la base de datos");
			}
		} catch (BDSoportesException e) {
			logger.error("Error insertando la planilla "+regTp01.getNroPlanillaAuditoria(),e);
		}
	}


	/**
	 * Método encargado de obtener la instancia de la clase que inserta la información 
	 *  de los activos en la tabla act_planilla_apte_tots 
	 * @param regTp01 Instancia de la clase {@link RegMinps01Bean} que contiene la información 
	 *  que se logro obtener de la ruta al archivo MINPS
	 */
	private static void insertarAct(RegMinps01Bean regTp01) {

		FaltantesActUpsertDAO dao = null;

		try {

			dao = FaltantesActUpsertDAO.getInstance();
			if(dao != null){
				dao.insertarRegTp1Act(regTp01);
			}else{
				logger.error("Error obteniendo instancia de la clase que inserta la información de activos en la base de datos");
			}

		} catch (BDSoportesException e) {
			logger.error("Error insertando la planilla "+regTp01.getNroPlanillaAuditoria(),e);
		}
	}
}
