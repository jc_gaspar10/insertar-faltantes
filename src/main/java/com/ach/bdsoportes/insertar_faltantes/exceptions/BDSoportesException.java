package com.ach.bdsoportes.insertar_faltantes.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.ach.bdsoportes.insertar_faltantes.enums.EstadoProcesoEnum;


/**
 * 
 * Clase para manejo de excepciones
 * 
 * @author jairo@alejandria-consulting.com
 *
 */
public class BDSoportesException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idEjecucion;
	private String nroPlanilla;
	private String identificacionAportante;
	private String detalleMensaje;
	private Date fechaPago;
	private String tipoMensaje = EstadoProcesoEnum.ERROR.toString();
	private int nroLinea;
	
	private Collection<BDSoportesException> multiples = new ArrayList<BDSoportesException>();
	
	public BDSoportesException ( BDSoportesException[] excepciones ){
		for ( BDSoportesException e:excepciones ){
			multiples.add(e);
		}
	}
	
	public boolean isMultiple (  ){
		return !multiples.isEmpty();
	}
	
	public Collection<BDSoportesException> getBDSoportesExceptions ( ){
		return multiples;
	}
	
	public BDSoportesException ( String mensaje, Exception original ){
		super(mensaje, original);
		setDetalleMensaje(original);
	}
	
	public BDSoportesException ( String mensaje ){
		super(mensaje);
	}
	
	public BDSoportesException ( String mensaje, String nroPlanilla, String identificacionAportante, Date fechaPago ,Exception original ){
		super(mensaje, original);
		this.nroPlanilla = nroPlanilla;
		this.identificacionAportante = identificacionAportante;
		this.fechaPago = fechaPago;
		setDetalleMensaje(original);
	}
	
	public BDSoportesException ( String mensaje, String idEjecucion,String nroPlanilla, String identificacionAportante, Date fechaPago ,Exception original ){
		super(mensaje, original);
		this.idEjecucion = idEjecucion;
		this.nroPlanilla = nroPlanilla;
		this.identificacionAportante = identificacionAportante;
		this.fechaPago = fechaPago;
		setDetalleMensaje(original);
	}
	
	public BDSoportesException ( String mensaje, String idEjecucion,String nroPlanilla, String identificacionAportante, Date fechaPago,int nroLinea, Exception original ){
		super(mensaje, original);
		this.idEjecucion = idEjecucion;
		this.nroPlanilla = nroPlanilla;
		this.nroLinea = nroLinea;
		this.identificacionAportante = identificacionAportante;
		this.fechaPago = fechaPago;
		setDetalleMensaje(original);
	}


	public BDSoportesException ( String mensaje, String idEjecucion,String nroPlanilla, String identificacionAportante, 
									Date fechaPago,int nroLinea, String tipo ){
		super(mensaje);
		this.nroPlanilla = nroPlanilla;
		this.idEjecucion = idEjecucion;
		this.nroLinea = nroLinea;
		this.identificacionAportante = identificacionAportante;
		this.fechaPago = fechaPago;
		this.tipoMensaje = tipo;
		
	}
	
	public BDSoportesException ( String mensaje, String idEjecucion,String nroPlanilla, String identificacionAportante, 
									Date fechaPago,int nroLinea, String tipo ,Exception original ){
		super(mensaje, original);
		this.nroPlanilla = nroPlanilla;
		this.idEjecucion = idEjecucion;
		this.nroLinea = nroLinea;
		this.identificacionAportante = identificacionAportante;
		this.fechaPago = fechaPago;
		this.tipoMensaje = tipo;
		setDetalleMensaje(original);
	}


	public BDSoportesException ( String mensaje, String idEjecucion,String nroPlanilla, String identificacionAportante, Date fechaPago,int nroLinea ){
		super(mensaje);
		this.idEjecucion = idEjecucion;
		this.nroPlanilla = nroPlanilla;
		this.nroLinea = nroLinea;
		this.identificacionAportante = identificacionAportante;
		this.fechaPago = fechaPago;
		this.idEjecucion = idEjecucion;
	}
	
	public BDSoportesException ( String mensaje, String nroPlanilla, String identificacionAportante, Date fechaPago){
		super(mensaje);
		this.nroPlanilla = nroPlanilla;
		this.identificacionAportante = identificacionAportante;
		this.fechaPago = fechaPago;
	}
	
	public BDSoportesException ( String mensaje, String idEjecucion, String nroPlanilla, String identificacionAportante, Date fechaPago){
		super(mensaje);
		this.idEjecucion = idEjecucion;		
		this.nroPlanilla = nroPlanilla;
		this.identificacionAportante = identificacionAportante;
		this.fechaPago = fechaPago;
	}

	public BDSoportesException ( String mensaje, String nroPlanilla, String identificacionAportante){
		super(mensaje);
		this.nroPlanilla = nroPlanilla;
		this.identificacionAportante = identificacionAportante;
	}
	
	public BDSoportesException ( String mensaje, String idEjecucion,String nroPlanilla, String identificacionAportante){
		super(mensaje);
		this.nroPlanilla = nroPlanilla;
		this.idEjecucion = idEjecucion;
		this.identificacionAportante = identificacionAportante;
	}
	
	public BDSoportesException ( String mensaje, String nroPlanilla, String identificacionAportante, Exception original ){
		super(mensaje,original);
		this.nroPlanilla = nroPlanilla;
		this.identificacionAportante = identificacionAportante;
		setDetalleMensaje(original);
	}
	
	public BDSoportesException ( String mensaje, String idEjecucion, String nroPlanilla, String identificacionAportante, Exception original ){
		super(mensaje,original);
		this.idEjecucion = idEjecucion;
		this.nroPlanilla = nroPlanilla;
		this.identificacionAportante = identificacionAportante;
		setDetalleMensaje(original);
	}
	
	
	public BDSoportesException ( String mensaje, String nroPlanilla){
		super(mensaje);
		this.nroPlanilla = nroPlanilla;
	}
	
	public BDSoportesException ( String mensaje, String nroPlanilla,Exception original ){
		super(mensaje,original);
		this.nroPlanilla = nroPlanilla;
		setDetalleMensaje(original);
	}
	
	
	private void setDetalleMensaje ( Exception original ){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		original.printStackTrace(pw);
		this.detalleMensaje = sw.toString(); 
	}
	
	public String getNroPlanilla() {
		return nroPlanilla;
	}

	public void setNroPlanilla(String nroPlanilla) {
		this.nroPlanilla = nroPlanilla;
	}

	public String getIdentificacionAportante() {
		return identificacionAportante;
	}

	public void setIdentificacionAportante(String identificacionAportante) {
		this.identificacionAportante = identificacionAportante;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getIdEjecucion() {
		return idEjecucion;
	}

	public void setIdEjecucion(String idEjecucion) {
		this.idEjecucion = idEjecucion;
	}

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public int getNroLinea() {
		return nroLinea;
	}

	public void setNroLinea(int nroLinea) {
		this.nroLinea = nroLinea;
	}

	public String getDetalleMensaje() {
		return detalleMensaje;
	}

	public void setDetalleMensaje(String detalleMensaje) {
		this.detalleMensaje = detalleMensaje;
	}
	
	
	
}
