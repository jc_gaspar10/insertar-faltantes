# Insertar MINPSs #

El proyecto Java cumple una función básica. Intenta extraer la mayor cantidad de información de las rutas a los archivos MINPS que se reciben como parametro y luego la inserta en la base de datos cassandra

## Parámetros ##
El proyecto recibe un solo argumento que es la ruta del archivo csv que contiene las rutas a los archivos MINPS. 

Sin embargo, se le deben pasar dos parámetros a la JVM:

 - Ruta del archivo de Propiedades: La ruta al archivo de propiedades se debe pasar como parámetro a la JVM de la siguiente manera "-Dconfiguration.properties=C:\User\Desktop\file.properties". En el archivo de propiedades debe existir las siguientes propiedades:
 
 	-cassandra.write.cluster: Cluster Cassandra del que se hará la lectura
	
	-cassandra.write.user: Usuario que se usará para ingresar al host Cassandra
	
	-cassandra.write.pass: Contraseña del Usuario con el que se ingresará al host Cassandra
	
	-cassandra.write.port: Puerto por el que se conectará al host Cassandra
	
	-cassandra.write.ips: Dirección de los nodos a los que se van a enviar las declaraciónes de escritura
	
	-cassandra.write.balancing.policy: Politica de balanceo a utilizar
	
	-cassandra.write.consistency.level: Nivel de consistencia que usaran las declaraciones 
 
 - Ruta del archivo de configuración log4j: La ruta al archivo que se encarga de la configuración de Log4j se debe pasar como parámetro a la JVM de la siguiente manera "-Dlog4j.configurationFile=C:\User\Desktop\log4jconfig.xml ". Consultar sobre este archivo en: https://logging.apache.org/log4j/2.x/manual/configuration.html

## Ejecución ##
La ejecución se realiza ejecutando el siguiente comando:

```bash
 java -Dconfiguration.properties="/ruta/al/properties" -Dlog4j.configurationFile="/ruta/al/archivo/config" -jar bdsoportes-insertar-minps.jar [archivo.csv]
```
## Resultado ##
Al ejecutar el proyecto correctamente debería crearse un archivo .log con el nombre que se le dio en el archivo de configuración de log4j. Si el log se deja en un nivel de debug se debería apreciar todo el proceso.
Además debe agregar a la base de datos cassandra que se configuró en el properties la información de que se tomó de las rutas a los archivos MINPS